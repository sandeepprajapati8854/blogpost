---
title: Sorting in JavaScript
date: 2023-03-06 17:18:15
tags:
---

## Indroduction

JavaScript has a built in sort() method that sorts elements in an array and returns the sorted array. The sort() method is a useful higher-order function that is mainly used with arrays in JavaScript.

In this article, we will learn about the sort method in JavaScript with some practical examples. Let's get right into it.

## The rule of the sort method

JavaScript has a built in sort method that sorts elements in an array and returns the sorted array. The sort method can optionally take a compare function where you can define how to sort the elements. If no compare function is provided, the elements are sorted ascending by converting the elements into strings and comparing their values.

Let’s look at an example of this:

```
const fruits = ["banana", "apple", "grapes", "strawberry", "orange"];
fruits.sort();
console.log(fruits);
//  ["apple", "banana", "grapes", "orange", "strawberry"]
const nums = [5, 1, 11, 200, 4];
nums.sort();
console.log(nums);
//  [1, 11, 200, 4, 5]
```

**The fruits get sorted as expected, alphabetically. However, the numbers do not get sorted numerically. Even though 4 is less than 11 numerically, the string “11” comes before the string “4” according to the Unicode order.**

When no compare function is provided, the default comparison follows these rules upon converting the elements into strings:

```
const nums = [5, 1, 11, 200, 4];
let afterSorting=nums.sort(function (number1, number2) {
  if (number1 < number2) {
    return -1;
  }
  if (number1 > number2) {
    return 1;
  }
  return 0;
});

console.log(afterSorting);

// [ 1, 4, 5, 11, 200 ]
```

### How sort callback function work.

**If number1 is less than number2, sort number1 to a lower index than number2, essentially making it first. If number1 is greater than number2, sort a to a higher index than number2, making it second. Otherwise, if number1 equals number2, keep both elements in place.**

Going back to our fruit example above, we can easily sort the fruits ascending by using the default compare function (fruits.sort()), but if we want to sort **descending**, we are going to have to pass in a custom compare function:

```
const fruits = ["banana", "apple", "grapes", "strawberry", "orange"];
let sortingFruitsDescending = fruits.sort(function (item1, item2) {
  if (item1 < item2) {
    return 1;
  }
  if (item1 > item2) {
    return -1;
  }
  return 0;
});
console.log(fruits);
// ["strawberry", "orange", "grapes", "banana", "apple"]
```

To sort elements numerically ascending, we can pass in a custom compare function that subtracts second from first

```
const numsArray = [5, 1, 11, 200, 4];
numsArray.sort((first, second) => first - second);
console.log(numsArray);
// => [1, 4, 5, 11, 200]
```

To sort numerically descending, we can just switch the subtraction and subtract first from second

```
const numsArray = [5, 1, 11, 200, 4];
numsArray.sort((first, second) => second - first);
console.log(numsArray);
// [ 200, 11, 5, 4, 1 ]
```

### How to sort a String

Imagine you were given a string and you had to sort that string alphabetically using a function. Example:

```
sortAlphabets( 'drpoklj' ); //returns 'djklopr'
```

**What would be the best way to do this?**

Then you can use array sort functions.

```

function sortString(str) {
  return str.split("").sort().join("");
}
let str = "drpoklj";
console.log(sortString(str));

// djklopr
```

**STEPS**

- Convert string to array
- Sort array
- Convert back array to string

## Sorting array of float point number

I have an array of float point number

```
let array=[ 82.11742562118049, 28.86823689842918, 49.61295450928224, 5.861613903793295 ];
```

After running sort() on the array I get this:

```
let arr = [
  82.11742562118049, 28.86823689842918, 49.61295450928224, 5.861613903793295,
];
function sortingFloating(arr) {
  return arr.sort();
}
console.log(sortingFloating(arr));
```

After running sort() on the array I get this:

```
[
  28.86823689842918,
  49.61295450928224,
  5.861613903793295,
  82.11742562118049
]
```

Notice how 5.8... is bigger than 49.6... for JavaScript (Node). Why is that?

Answer :- Use arr.sort(function (a, b) { return a-b; });. As it stands, the values are being sorted alphabetically. "2" comes before "4", which comes before "5", which comes before "8" (the comparison is the first "letter" of each number...until they match, then it compares the next letter, and so on)

```
let arr = [
  82.11742562118049, 28.86823689842918, 49.61295450928224, 5.861613903793295,
];
function sortingFloating(arr) {
  let afterSorting = arr.sort(function (first, second) {
    return first - second;
  });
  console.log(afterSorting);
}
sortingFloating(arr);

// [
  5.861613903793295,
  28.86823689842918,
  49.61295450928224,
  82.11742562118049
]
```

We can also sort an array of objects based on a specific object property, by age or by name.

```
const object = [
  { name: "Amy", age: 26 },
  { name: "Bob", age: 50 },
  { name: "Steve", age: 21 },
  { name: "John", age: 33 },
  { name: "Elizabeth", age: 16 },
];

object.sort((first, second) => first.age - second.age);
console.log(object);
// output
[
  { name: 'Elizabeth', age: 16 },
  { name: 'Steve', age: 21 },
  { name: 'Amy', age: 26 },
  { name: 'John', age: 33 },
  { name: 'Bob', age: 50 }
]
```

### Sort by name

```
const object = [
  { name: "Amy", age: 26 },
  { name: "Bob", age: 50 },
  { name: "Steve", age: 21 },
  { name: "John", age: 33 },
  { name: "Elizabeth", age: 16 },
];

object.sort((first, second) => {
  if (first.name < second.name) {
    return -1;
  }
  if (first.name > second.name) {
    return 1;
  }
  return 0;
});
console.log(object);

// Output:-
[
  { name: 'Amy', age: 26 },
  { name: 'Bob', age: 50 },
  { name: 'Elizabeth', age: 16 },
  { name: 'John', age: 33 },
  { name: 'Steve', age: 21 }
]
```

### Multiple Keys in Object.

```
let object = [
  { name: "Mark", age: 30, RollNo: "R01" },
  { name: "Anne", age: 20, RollNo: "R02" },
  { name: "James", age: 40, RollNo: "R03" },
  { name: "Jerry", age: 30, RollNo: "R04" },
  { name: "Lucy", age: 30, RollNo: "R05" },
  { name: "Mark", age: 30, RollNo: "R06" },
];
```

We can combine sorts using the || operator in the order of the sorting we need.

Sort by Age, and then Name

```
object.sort(
  (first, second) =>
    first.age - second.age || first.name.localeCompare(second.name)
);

console.log(object);

// Output
[
  { name: 'Anne', age: 20, RollNo: 'R02' },
  { name: 'Jerry', age: 30, RollNo: 'R04' },
  { name: 'Lucy', age: 30, RollNo: 'R05' },
  { name: 'Mark', age: 30, RollNo: 'R01' },
  { name: 'Mark', age: 30, RollNo: 'R06' },
  { name: 'James', age: 40, RollNo: 'R03' }
]

```

### JavaScript String localeCompare()

**Definition and Usage**

- The localeCompare() method compares two strings in the current locale.

- The localeCompare() method returns sort order -1, 1, or 0 (for before, after, or equal).

**Syntax**

```
string.localeCompare(compareString)
```

**Return value**

- -1 if the string is sorted before the compareString
- 0 if the two strings are equal
- 1 if the string is sorted after the compareString

Thank you for reading.
