---
title: pass by value and pass by reference
date: 2023-03-02 09:03:19
tags:
---

## Overview

In JavaScript, when a function is called, the arguments can be passed in two ways, either Pass by value or Pass by reference (address). Primitive data types such as string, number, null, undefined, and boolean, are passed by value while non-primitive data types such as objects, arrays, and functions are passed by reference in Javascript

### Let's quick look of Primitive & Non-Primitive Data Types

let's understand what primitive and non-primitive data types are. So, in JavaScript, the data types are divided into two broad categories:

- Primitive
- Non-primitive

**A primitive is data that is not an object and has no method are properties. that is called primitive data type**

```

1-  string     4- number
2-  null       5- boolean
3-  symbol     6- undefined
```

**Non-primitive data types are called reference types because they refer to objects.**

```
1- Object
2- Array
3- Function
```

#### Note.

The main difference between primitives & non-primitives is that primitives are immutable , there is no way to change a primitive value once it gets created, whereas non-primitives are mutable , the value of an object can be changed after it gets created.

#### Example of Primitives data types

```
let companyName = 'Mountblue Technologies';
companyName[10] = 't'
console.log(string) // Mountblue Technologies

```

```
let myString = "Mountblue";
console.log(myString); // Mountblue

myString = myString + " Technologies";
console.log(myString); // Mountblue Technologies

```

In the example given above, first, we assigned Mountblue to 'myString' then we console logged it, and we got Mountblue in the console, then we appended the value " Technologies" to the existing value of 'myString' and on console logging it, we got Mountblue Technologies in the console. Now you may ask how it can happen as I already mentioned that primitive data types are immutable, and string is one of them?

**To understand this, we need to break the process into steps:**

- first, the existing value of "myString" is fetched, "Mountblue".
- Then " Technologies" is appended to that existing value.
- Then the resultant value, "Mountblue Technologies" is allocated to a new block of memory.

- Now, the "myString" object points to the newly created memory space (the space to which the resultant value is allocated in the third step)

- Now, the previously created memory space the space of myString = "String") is available for garbage collection.

#### note:-

**Primitive data types are compared by value. If two values are the same, then they are strictly equal.**

```
let num1 = 40;
let num2 = 40;
num1 === num2; // true

let string1 = 'Mountblue Technologies';
let string2 = 'Mountblue Technologies';
string1 === string2; // true
```

#### Non-primitive Data Types

```
let myArray = ["Mountblue" ,"Technologies" ];
myArray[1] = 'Unicorn';
console.log(myArray) // [ 'Mountblue', 'Unicorn' ];
```

#### note:-

**Objects and arrays are not compared by value. That means even if two objects and arrays have the same values and properties or the same elements, respectively, they are not strictly equal.**

```
var obj1 = {
    'website': 'Mountblue Technologies',
    'topic': 'JavaScript'
    };

var obj2 = {
   'website': 'Mountblue Technologies',
   'topic': 'JavaScript'
    };

obj1 === obj2;  // false

var myArray1 = [ 'Mountblue', 'Unicorn' ];
var myArray2 = [ 'Mountblue', 'Unicorn' ];
arr1 === arr2;  // false

```

#### note:-

**Two objects are strictly equal only if they refer to the same object.**

```
var obj1 = {
    'website': 'Mountblue Unicorn',
    'topic': 'JavaScript'
    };

var obj2 = obj1;
obj1 === obj2;  // true

```

Non-primitive values are sometimes also referred to as reference types because instead of values, they are compared by reference.

**Note:** In JavaScript, primitive values are stored on the stack, while non-primitive values are stored in a heap.

### Pass by Value

Pass by value in JavaScript means that a copy of the actual parameter’s value is made in memory , a new memory allocation is done, and all the changes are made in that new value copied value. The original value and the copied value are independent of each other as they both have a different space in memory on changing the value inside the function, the variable outside the function is not affected.

In simple language, we can understand it as, in a pass-by value, the function receives a copy of the variable, which is independent of the originally passed variable.

Pass by value in JavaScript requires more space as the functions get a copy of the actual content therefore, a new variable is created in the memory.

```
let num1 = 70
let num2 = num1

console.log(num1) // 70
console.log(num2) // 70

num1 = 40

console.log(num1) // 40
console.log(num2) // 70
```

Here, we have assigned num1 a value of 70. This creates a space in memory by the name num1 and addresses 2001 (assumption). When we create a variable num2 and assign it the value of num1, then equals operator notices that we’re dealing with a primitive values thus it creates a NEW SPACE in memory with address 2002 and assigns it a copy of num1's value, 70. Now we can see that both the variables have different spaces in the memory, and both have a value of 70.

Now, if we change the value of num1, then num2 will have no effect as it has its own separate space in memory and now it has nothing to do with the value of num2 as they both have different spaces (address) in memory.

**Let's understand this better by another example:**

```
function multiplication(tmp) {
    tmp = tmp * 50;
    return tmp;
}
var num = 30;
var result = multiplication(num);
console.log(num); // 30
console.log(result); // 1500

```

Then we have declared a variable num, with a value of 30.

After that, we passed the variable num to the multiplication function. Javascript automatically copies the value of variable num to variable tmp. So, here tmp is a new variable that is allocated a new space in the memory and is independent of num.

Now all the changes made by the function multiplication are done directly to the variable tmp; thus the value of num remains unaffected.

This happens because a separate copy of variable num is created in the memory named tmp with initial value 30, which after calculation becomes 1500.

and finally tmp and num have no link with each other, they are independent of each other.

### Pass by Reference

Pass by reference in JavaScript does not create a new space in the memory,

we pass the reference/address of the actual parameter, which means the function can access the original value of the variable. Thus, if we change the value of the variable inside the function, then the original value also gets changed.

It does not create a copy, instead, it works on the original variable, so all the changes made inside the function affect the original variable as well.

```
let obj1 = {website: 'Mountblue Technologies'}
let obj2 = obj1;

console.log(obj1)     // {website: 'Mountblue Technologies'}
console.log(obj2)     // {website: 'Mountblue Technologies'}

obj1.website = "Mountblue Unicorn"

console.log(obj1)     // {website: "Mountblue Unicorn"}
console.log(obj2)     // {website: "Mountblue Unicorn"}

```

As the equal operator identifies that we are dealing with non-primitive data types, so instead of creating a new memory space, it points obj2 to the same memory space that obj1 is pointed to. Thus when we change (mutate) the value of obj1, then the value of obj2 also gets changed since obj2 is also pointing to the same memory space as obj1 does.

## When to Use Pass by Value?

As in pass-by value in JavaScript, a new copy of the variable is created, and any changes made in the new variable are independent of the original variable, so it is useful when we want to keep track of the initial variable and don't want to lose its value.

## When to Use Pass by Reference?

When we are passing arguments of large size, it is better to use pass-by-reference in JavaScript as no separate copy is made in the called function, so memory is not wasted, and hence the program is more efficient.

Thank you.
