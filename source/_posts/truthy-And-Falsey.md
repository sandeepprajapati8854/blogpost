---
title: Truthy-And-Falsey
date: 2023-02-22 13:55:42
tags:
---

<!-- # Truthy and Falsy Values in Javascript -->

Here are some basic and tricky fundamental topics in JavaScript. This is common information and this is present elsewhere online. This post is only for study purposes.
<br>
<br>

**Truthy:** Represent TRUE

**Falsey:** Represent FALSE

It's completely logical. Zero (0) is false. One (1) is true

There are only six false values in JavaScript:

- null
- NaN
- 0
- "" (empty string)
- undefined
- false.

Why do people care what's true and what's false?

A true value always determines what it will evaluate in logical conditions. For example

```
function checkingTrueOrFalse(){
    if(0){
        console.log("Welcome to Mountblue Technologies")
    }
}
checkingTrueOrFalse();
```

We already know that the console.log() never will run, because of 0 (false), the condition will not satisfy.
The condition only runs if the passed condition is NOT false.

Another True case:

```
function checkingTrueOrFalse(){
    if(1){
        console.log("Welcome to Mountblue Technologies")
    }
}
checkingTrueOrFalse();
```

In this function, the console.log() Always will run, because of 1 (true), and he will print based on your console.

How to perform multiple conditions based on true and false values?

```
console.log(undefined == false);
console.log(null == false);
console.log(Boolean == false);
console.log(Object == false);
console.log(Number == false);
console.log(String == false);
```

Note:- In the console, all value is false.

If you want to perform multiple conditions based on the true and false value you have to go to Logic Gate.

- AND Gate.

This Logic gate will give the true value when both conditions are true.

|  0  |  0  |  0  |
| :-: | :-: | :-: |
|  0  |  1  |  0  |
|  1  |  0  |  0  |
|  1  |  1  |  1  |

This condition will execute when all conditions become true.

Example.

```
function operationANDGate(num){
    if(num%3==0&&num%5==0){
        console.log("Your Welcome")
        }else{
            console.log("Try another number")
        }
}
let num=15;
operationANDGate(num);

```

Here we can see that when num%3==0 and num%5==0 become true the condition will execute.

Thank You!
